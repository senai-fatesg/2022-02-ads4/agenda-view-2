import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

  constructor(private httpClient: HttpClient) { }

  get(): Observable<any>{
    return this.httpClient.get(`${environment.url}/contato`);
  }

  delete(id: any): Observable<any>{
    return this.httpClient.delete(`${environment.url}/contato/${id}`);
  }

  post(contato: any): Observable<any> {
    return this.httpClient.post(`${environment.url}/contato`, contato);
  }


}
