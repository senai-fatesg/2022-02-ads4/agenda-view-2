import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { LoginService } from '../../login.servce';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private loginService: LoginService) {}

  ngOnInit() {
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  login(){
    let login = this.form.get('uname').value;
    let senha = this.form.get('password').value;
    localStorage.removeItem("ads_access_token");
    this.loginService.login(login, senha).subscribe(r =>{
      let token = r.access_token;
      let email = r.login;
      localStorage.setItem("ads_access_token", token);
      this.router.navigate(['/apps/contato']);
    });
  }


}
