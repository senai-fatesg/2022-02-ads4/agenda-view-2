import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { LoginService } from '../../login.servce';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  login:string = "";
  senha:string = "";
  confirmaSenha:string = "";
  nome:string = "";
  cpf:string = "";
  concorda:boolean=false;

  mensagem:string = "";

  constructor(private router: Router, private loginService:LoginService) {}

  ngOnInit() {
  }

  criar(){
    if(this.validarCampos()){
      this.loginService.criar(this.login, this.senha, this.cpf, this.nome).subscribe(r =>{
        this.router.navigate(['/']);
      });
    }
  }

  desabilitaBotaoCriar(): boolean{
    return !this.concorda || this.senha === "" || this.senha !== this.confirmaSenha;
  }

  validarCampos():boolean{
    let resp:boolean = true;
    if(this.login === ""){
      this.mensagem += "Login obrigatório; ";
      resp = false;
    }
    if(this.cpf === ""){
      this.mensagem += "cpf obrigatório; ";
      resp = false;
    }
    if(this.nome === ""){
      this.mensagem += "Nome obrigatório; ";
      resp = false;
    }
    return resp;
  }

}
