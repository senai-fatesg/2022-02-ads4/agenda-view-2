import { Routes } from '@angular/router';

import { ContatoComponent } from './contato/contato.component';
import { AuthGuardService } from '../auth-guard-service';
export const AppsRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'contato',
        component: ContatoComponent,
        canActivate: [AuthGuardService]
      },
    ]
  }
];
