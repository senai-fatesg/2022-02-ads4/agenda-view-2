import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { ContatoService } from '../../contato.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.css']
})
export class ContatoComponent implements OnInit {

  public contato:any = {"nome": '', "telefone": ''};
  public contatos:any = [];

  dataSource: MatTableDataSource<Contato>;

  @ViewChild(MatSort, {static:true}) sort: MatSort;

  displayedColumns = ['id', 'nome', 'telefone'];
  
  constructor(private contatoService: ContatoService) { }

  ngOnInit() {
    

    console.log('Passo 1');
    this.listar();
    console.log('Passo 3');
    
    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  listar(){
    this.contatoService.get().subscribe(r => {
      this.dataSource = new MatTableDataSource(r);
      this.dataSource.sort = this.sort;
      console.log('Passo 2');
    });
  }

  addContato(){
    this.contatoService.post(this.contato).subscribe(r => {
      this.listar();
      this.contato =  {"nome": '', "telefone": ''}
    });
  }

  delete(id: any){
    this.contatoService.delete(id).subscribe(r => {
      this.listar();
    });
  }

  selecionarContato(c: any){
    this.contato = c;
  }  
}

export interface Contato{
  id: number;
  telefone: string;
  nome: string;
}
