import { Routes } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import { ContatoComponent } from './apps/contato/contato.component';
import { AuthGuardService } from './auth-guard-service';


export const AppRoutes: Routes = [
  {
    path: '',
    component: FullComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'app/contato',
        pathMatch: 'full',
      },
      {
        path: 'apps',
        loadChildren: './apps/apps.module#AppsModule',
      },
    ]
  },
  {
    path: '',
    component: AppBlankComponent,
    children: [
      {
        path: 'authentication',
        loadChildren:
          './authentication/authentication.module#AuthenticationModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'authentication/404'
  }
];
