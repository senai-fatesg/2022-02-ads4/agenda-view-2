import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {
  
    constructor(public router: Router) {}

  canActivate(): boolean {
    let token = localStorage.getItem("ads_access_token");
    console.log(token);
    if (!token) {
      this.router.navigate(['authentication/login']);
      return false;
    }
    return true;
  }
}